--Full Name, Gender, State, City, Street, Zip code, Birth Year,Email, Password

CREATE TABLE IF NOT EXISTS public.profile(
    Id SERIAL,
    FirstName varchar(255),
    MiddleName varchar(255),
    LastName varchar(255),
    Email varchar(50),
    BirthDate date,
    PRIMARY KEY (Id)
);