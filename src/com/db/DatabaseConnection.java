package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	
	private static DatabaseConnection databaseConnection;
	private static final String USER = "postgres";	
	private static final String PASSWORD = "admin";
	private static final String DRIVER_URL = "org.postgresql.Driver";
	private static final String DB_URL = "jdbc:postgresql://localhost:5432/carpool";
	
	private Connection connection;
	
	private DatabaseConnection(){
		initializeConnection();
	}
	
	public static DatabaseConnection getInstance() {
		if (databaseConnection == null) {
			databaseConnection = new DatabaseConnection();			
		}			
		return databaseConnection;
	}
	
	private void initializeConnection(){
		
		//Postgre
        try {
        	
			Class.forName(DRIVER_URL);
			System.out.println("Initialize JDBC Driver!");
			
			connection = DriverManager.getConnection(DB_URL,USER, PASSWORD);
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
        catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Connecting to database...");
		
	}
	
	public void closeConnection() {
		try {
			System.out.println("Closing connection...");
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		DatabaseConnection conn = DatabaseConnection.getInstance();
		conn.closeConnection();
	}
	
	
}
